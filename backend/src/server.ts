import express, { Application, Router } from 'express';
import pool from './db/dbconnector';
import * as router from './routers/router'

class Server {
    private app;

    constructor() {
        this.app = express();
        this.config();
        this.routerConfig();
        this.dbConnect();
    }

    private config() {
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(express.json());
    }

    private dbConnect() {
        pool.connect(function (err, client, done) {
            if (err) throw new Error(err.message);
            console.log('Connected');
          }); 
    }

    private routerConfig() {
        router.register(this.app)
    }

    public start = (port: number) => {
        return new Promise((resolve, reject) => {
            this.app.listen(port, () => {
                resolve(port);
            }).on('error', (err: Object) => reject(err));
        });
    }
}

export default Server;