import * as express from "express";
import apiRouter from './api.js'

export const register = (app: express.Application) => {

    app.get("/", (req, res) => {
        res.sendFile("./dist/index.html", {root: "../frontend"})
    });

    app.get("/assets/*", (req, res) => {
        res.sendFile("./dist" + req.path, {root: "../frontend"})
    });

    app.use("/api", apiRouter)
};