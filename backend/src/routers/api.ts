import * as express from "express";
import AthleteController from "../controllers/AthleteController";
import ClubController from "../controllers/ClubController";
import CompetitionController from "../controllers/CompetitionController";

const router = express.Router();
const athleteControler = new AthleteController();
const clubControler = new ClubController();
const competitionControler = new CompetitionController();

router.get("/search", async (req, res) => {
});

router.get("/clubs",  clubControler.requestAll);
router.post("/clubs",  clubControler.create);
router.get("/club/:id", clubControler.requestById);
router.get("/club/:id/athletes", clubControler.requestAthletes);

router.get("/athletes", athleteControler.requestAll);
router.post("/athletes", athleteControler.create);
router.get("/athlete/:id", athleteControler.requestById);
router.post("/athlete/:id", athleteControler.updateById);
router.delete("/athlete/:id", athleteControler.deleteById);
router.get("/athlete/:id/club", athleteControler.requestCurrentClub);
router.post("/athlete/:id/club", athleteControler.updateCurrentClub);
router.get("/athlete/:id/clubs", athleteControler.requestClubs);
router.get("/athlete/:id/results", athleteControler.requestResults);
router.post("/athlete/:id/results", athleteControler.updateResults);
router.delete("/athlete/:id/result/:rid", athleteControler.deleteResult);
router.get("/athlete/:id/competitions", athleteControler.requestCompetitions);

router.get("/competitions", competitionControler.requestAll);
router.get("/competition/:id", competitionControler.requestById);
router.get("/competition/:id/athletes", competitionControler.requestAthletes);

export default router;