import { Pool } from 'pg';

export default new Pool ({
    max: 20,
    connectionString: 'postgres://postgres:bazepodataka@localhost:5432/infsus-test4',
    idleTimeoutMillis: 30000
});