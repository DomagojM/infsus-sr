import * as express from "express";
import clubModel from '../models/ClubModel'

class ClubController {

    public async requestAll(req: express.Request, res: express.Response) {
        const comp = await clubModel.getAll()

        if(comp)
            res.send(comp);
        else
            res.sendStatus(400);
    }

    public async create(req: express.Request, res: express.Response) {
        const body = req.body;

        if(body.clubid.length < 1 || body.clubname.length < 2 || body.clubprefix < 2)
            return res.sendStatus(400)

        const comp = await clubModel.create(body.clubid, body.clubname, body.clubprefix)

        if(comp)
            res.sendStatus(200)
        else
            res.sendStatus(400);
    }

    public async requestById(req: express.Request, res: express.Response) {
        const id = req.params['id'];
        const comp = await clubModel.getById(id.toUpperCase())

        if(comp) {
            if(comp.length == 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async requestAthletes(req: express.Request, res: express.Response) {
        const id = req.params['id'];
        const comp = await clubModel.getAthletes(id.toUpperCase())

        if(comp) {
            if(comp.length >= 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }
}

export default ClubController;