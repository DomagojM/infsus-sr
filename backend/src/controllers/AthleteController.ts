import * as express from "express";
import athleteModel from '../models/AthleteModel'

class AthleteController {

    public async requestAll(req: express.Request, res: express.Response) {
        const comp = await athleteModel.getAll()

        if(comp)
            res.send(comp);
        else
            res.sendStatus(400);
    }

    public async create(req: express.Request, res: express.Response) {
        const body = req.body;

        if(body.firstname.length < 2 || body.lastname.length < 2 || new Date(body.date) > new Date())
            return res.sendStatus(400)

        const comp = await athleteModel.create(body.firstname, body.lastname, body.birthday, body.clubid)

        if(comp)
            res.sendStatus(200)
        else
            res.sendStatus(400);
    }

    public async requestById(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await athleteModel.getById(id)

        if(comp) {
            if(comp.length == 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async updateById(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const body = req.body;

        const comp = await athleteModel.updateById(id, body.firstname, body.lastname, body.birthday)

        if(comp) {
            res.sendStatus(200)
        } else
            res.sendStatus(400);
    }

    public async deleteById(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);

        const comp = await athleteModel.deleteById(id)

        if(comp) {
            res.sendStatus(200)
        } else
            res.sendStatus(400);
    }

    public async requestCurrentClub(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await athleteModel.getCurrentClub(id)

        if(comp) {
            if(comp.length == 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async updateCurrentClub(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const body = req.body;

        // ----- Validacija -----
        let clublist = await athleteModel.getClubs(id)
        clublist = clublist?.map(c => c.clubid)
        if(clublist?.includes(body.clubid))
            return res.sendStatus(403)
        // ----------------------

        const comp = await athleteModel.updateCurrentClub(id, body.clubid)

        if(comp) {
            res.sendStatus(200)
        } else
            res.sendStatus(400);
    }

    public async requestClubs(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await athleteModel.getClubs(id)

        if(comp) {
            if(comp.length >= 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async requestResults(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await athleteModel.getResults(id)

        if(comp) {
            if(comp.length >= 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async updateResults(req: express.Request, res: express.Response) {
        const athleteid = parseInt(req.params['id']);
        const body = req.body;

        var comp;
        var comp2;
        if(body.resultid >= 0) {
            comp = await athleteModel.updateResults(athleteid, body.resultid, body.discipline, body.note)
            if(body.score)
                comp2 = await athleteModel.updateValueResults(body.resultid, body.score, body.unit)
            else if (body.time)
                comp2 = await athleteModel.updateTimeResults(body.resultid, body.time)
        } else {
            comp = await athleteModel.addResults(athleteid, body.discipline, body.note)
            if(body.score)
                comp2 = await athleteModel.addValueResults(comp!, body.score, body.unit)
            else if (body.time)
                comp2 = await athleteModel.addTimeResults(comp!, body.time)
        }

        if (comp) {
            res.sendStatus(200)
        } else
            res.sendStatus(400);
    }

    public async deleteResult(req: express.Request, res: express.Response) {
        const athleteid = parseInt(req.params['id']);
        const resultid = parseInt(req.params['rid']);
        
        const comp = await athleteModel.deleteResult(resultid);
        if (comp) 
            res.sendStatus(200)
        else
            res.sendStatus(400);
    }

    public async requestCompetitions(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await athleteModel.getCompetitions(id)

        if(comp) {
            if(comp.length >= 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }
}

export default AthleteController;