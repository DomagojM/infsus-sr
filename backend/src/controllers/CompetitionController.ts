import * as express from "express";
import competitionModel from '../models/CompetitionModel'

class CompetitionController {

    public async requestAll(req: express.Request, res: express.Response) {
        const comp = await competitionModel.getAll()

        if(comp)
            res.send(comp);
        else
            res.sendStatus(400);
    }

    public async requestById(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await competitionModel.getById(id)

        if(comp) {
            if(comp.length == 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }

    public async requestAthletes(req: express.Request, res: express.Response) {
        const id = parseInt(req.params['id']);
        const comp = await competitionModel.getAthletes(id)

        if(comp) {
            if(comp.length >= 1)
                res.send(comp);
            else if(comp.length == 0)
                res.sendStatus(404);
            else
                res.sendStatus(500);
        } else
            res.sendStatus(400);
    }
}

export default CompetitionController;