import pool from '../db/dbconnector';

const tableName = "athlete";
const tableId = "athleteID";

class AthleteModel {

    constructor(
        public firstName: string, 
        public lastName: string, 
        public birthday: Date
    ){}


    // ---------- Static metode ----------

    public static async getAll() {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName}`;
            const { rows } = await client.query(sql);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName}!`);
            console.error(error);
            return undefined;
        }
    }

    static async create(firstname: string, lastname: string, birthday: Date, clubid: string) {
        const locTable1 = "member";
        try {
            const client = await pool.connect();

            const sql1 = `INSERT INTO ${tableName} (firstname, lastname, birthday) VALUES($1, $2, $3) RETURNING athleteid;`;
            const { rows } = await client.query(sql1, [firstname, lastname, birthday]);
            
            const sql2 = `INSERT INTO ${locTable1} (athleteid, clubid, memberfrom, memberto) VALUES($1, $2, $3, NULL);`;
            await client.query(sql2, [rows[0].athleteid, clubid, new Date()]);

            client.release();

            return true;
        } catch (error) {
            console.error(error)
            return undefined;
        }
    }

    public static async getById(id: number) {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName} by id: ` + id + "!");
            console.error(error)
            return undefined;
        }
    }

    static async updateById(id: number, firstname: string, lastname: string, birthday: Date) {
        try {
            const client = await pool.connect();

            const sql = `UPDATE ${tableName} SET firstname = $1, lastname = $2, birthday = $3 WHERE ${tableId} = $4;`;
            const { rows } = await client.query(sql, [firstname, lastname, birthday, id]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch ${tableName} by id: ` + id + "!");
            console.error(error)
            return undefined;
        }
    }

    static async deleteById(id: number,) {
        const locTable1 = "member";
        const locTable2 = "compete";
        const locTable3 = "results";

        try {
            const client = await pool.connect();
            
            const sql1 = `DELETE FROM  ${locTable3} WHERE ${tableId} = $1;`;
            await client.query(sql1, [id]);
            
            const sql2 = `DELETE FROM  ${locTable2} WHERE ${tableId} = $1;`;
            await client.query(sql2, [id]);
            
            const sql3 = `DELETE FROM  ${locTable1} WHERE ${tableId} = $1;`;
            await client.query(sql3, [id]);
            
            const sql4 = `DELETE FROM  ${tableName} WHERE ${tableId} = $1;`;
            await client.query(sql4, [id]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch ${tableName} by id: ` + id + "!");
            console.error(error)
            return undefined;
        }
    }

    public static async getCurrentClub(id: number) {
        const locTable1 = "member";
        const locTable2 = "club";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable2}.* FROM ${tableName} NATURAL JOIN ${locTable1} NATURAL JOIN ${locTable2} WHERE memberto IS NULL AND ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async updateCurrentClub(id: number, clubid: any) {
        const locTable1 = "member";
        try {
            const client = await pool.connect();

            const sql1 = `UPDATE ${locTable1} SET memberto = $1 WHERE ${tableId} = $2 AND memberto IS NULL;`;
            await client.query(sql1, [new Date(), id]);

            const sql2 = `INSERT INTO ${locTable1} (athleteid, clubid, memberfrom, memberto) VALUES ($1, $2, $3, NULL);`;
            const { rows } = await client.query(sql2, [id, clubid, new Date()]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    public static async getClubs(id: number) {
        const locTable1 = "member";
        const locTable2 = "club";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable2}.*, ${locTable1}.memberfrom, ${locTable1}.memberto FROM ${tableName} NATURAL JOIN ${locTable1} NATURAL JOIN ${locTable2} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    public static async getResults(id: number) {
        const locTable1 = "results";
        const locTable2 = "timeresults";
        const locTable3 = "valueresults";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable1}.*, ${locTable3}.score, ${locTable3}.unit, ${locTable2}.time FROM ${tableName} NATURAL LEFT JOIN ${locTable1} NATURAL LEFT JOIN ${locTable2} NATURAL LEFT JOIN ${locTable3} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async updateResults(athleteid: number, resultid: number, discipline: string, note: any) {
        const locTable1 = "results";
        try {
            const client = await pool.connect();

            const sql = `UPDATE ${locTable1} SET discipline = $1, note = $2 WHERE athleteid = $3 AND resultid = $4;`;
            const { rows } = await client.query(sql, [discipline, note, athleteid, resultid]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async updateValueResults(resultid: number, score: number, unit: string) {
        const locTable1 = "valueresults";
        try {
            const client = await pool.connect();

            const sql = `UPDATE ${locTable1} SET score = $1, unit = $2 WHERE resultid = $3;`;
            const { rows } = await client.query(sql, [score, unit, resultid]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async updateTimeResults(resultid: number, time: string) {
        const locTable1 = "timeresults";
        try {
            const client = await pool.connect();

            const sql = `UPDATE ${locTable1} SET time = $1 WHERE resultid = $2;`;
            const { rows } = await client.query(sql, [time, resultid]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async addResults(athleteid: number, discipline: string, note: any) {
        const locTable1 = "results";
        try {
            const client = await pool.connect();

            const sql = `INSERT INTO ${locTable1} (athleteid, note, discipline, adminid) VALUES($1, $2, $3, 1) RETURNING resultid;`;
            const { rows } = await client.query(sql, [athleteid, note, discipline]);

            client.release();

            return rows[0].resultid;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async addValueResults(resultid: number, score: number, unit: string) {
        const locTable1 = "valueresults";
        try {
            const client = await pool.connect();

            const sql = `INSERT INTO ${locTable1} (resultid, score, unit) VALUES($1, $2, $3) RETURNING resultid;`;
            const { rows } = await client.query(sql, [resultid, score, unit]);

            client.release();

            return rows[0].resultid;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async addTimeResults(resultid: number, time: string) {
        const locTable1 = "timeresults";
        try {
            const client = await pool.connect();

            const sql = `INSERT INTO ${locTable1} (resultid, time) VALUES($1, $2) RETURNING resultid;`;
            const { rows } = await client.query(sql, [resultid, time]);

            client.release();

            return rows[0].resultid;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    public static async getCompetitions(id: number) {
        const locTable1 = "compete";
        const locTable2 = "competition";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable2}.* FROM ${tableName} NATURAL JOIN ${locTable1} NATURAL JOIN ${locTable2} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }

    static async deleteResult(resultid: number) {
        const locTable1 = "results";
        const locTable2 = "timeresults";
        const locTable3 = "valueresults";
        try {
            const client = await pool.connect();

            const sql1 = `DELETE FROM ${locTable2} WHERE resultid = $1;`;
            await client.query(sql1, [resultid]);
            
            const sql2 = `DELETE FROM ${locTable3} WHERE resultid = $1;`;
            await client.query(sql2, [resultid]);

            const sql3 = `DELETE FROM ${locTable1} WHERE resultid = $1;`;
            await client.query(sql3, [resultid]);

            client.release();

            return true;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }
}

export default AthleteModel;