import pool from '../db/dbconnector';

const tableName = "club";
const tableId = "clubID";

class ClubModel {

    constructor(
        public clubName: string, 
        public clubPrefix: string, 
        public sport: string
    ){}


    // ---------- Static metode ----------

    public static async getAll() {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName}`;
            const { rows } = await client.query(sql);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName}!`);
            console.error(error);
            return undefined;
        }
    }

    static async create(clubid: string, clubname: string, clubprefix: string) {
        try {
            const client = await pool.connect();

            const sql1 = `INSERT INTO ${tableName} (clubid, clubname, clubprefix, sportid) VALUES($1, $2, $3, 1);`;
            const { rows } = await client.query(sql1, [clubid.toUpperCase(), clubname, clubprefix.toUpperCase()]);

            client.release();

            return true;
        } catch (error) {
            console.error(error)
            return undefined;
        }
    }

    public static async getById(id: string) {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName} by id: ` + id + "!");
            console.error(error)
            return undefined;
        }
    }

    public static async getAthletes(id: string) {
        const locTable1 = "member";
        const locTable2 = "athlete";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable2}.*, ${locTable1}.* FROM ${locTable1} NATURAL JOIN ${tableName} NATURAL JOIN ${locTable2} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }
}

export default ClubModel;