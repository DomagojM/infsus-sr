import pool from '../db/dbconnector';

const tableName = "competition";
const tableId = "competitionID";

class CompetitionModel {

    constructor(
        public competitionName: string, 
        public startTime: Date, 
        public endTime: Date, 
        public sport: string
    ){}


    // ---------- Static metode ----------

    public static async getAll() {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName}`;
            const { rows } = await client.query(sql);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName}!`);
            console.error(error);
            return undefined;
        }
    }

    public static async getById(id: number) {
        try {
            const client = await pool.connect();

            const sql = `SELECT * FROM ${tableName} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch ${tableName} by id: ` + id + "!");
            console.error(error)
            return undefined;
        }
    }

    public static async getAthletes(id: number) {
        const locTable1 = "compete";
        const locTable2 = "athlete";
        try {
            const client = await pool.connect();

            const sql = `SELECT ${locTable2}.*, ${locTable1}.clubid FROM ${locTable1} NATURAL JOIN ${tableName} NATURAL JOIN ${locTable2} WHERE ${tableId} = $1;`;
            const { rows } = await client.query(sql, [id]);

            client.release();

            return rows;
        } catch (error) {
            console.error(`Unable to fetch from ${tableName}!`);
            console.error(error)
            return undefined;
        }
    }
}

export default CompetitionModel;