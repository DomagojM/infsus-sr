import { createRouter, createWebHistory } from 'vue-router'
import SearchListView from '../views/SearchListView.vue'
import AthleteView from '../views/AthleteView.vue'
import AthleteEditView from '../views/AthleteEditView.vue'
import CreateAthleteView from '../views/CreateAthleteView.vue'
import CreateClubView from '../views/CreateClubView.vue'
// import CreateCompetitionView from '../views/CreateCompetitionView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/search-list',
      name: 'search-list',
      component: SearchListView
    },
    {
      path: '/athlete/:id',
      component: AthleteView
    },
    {
      path: '/athlete/:id/edit',
      component: AthleteEditView
    },
    {
      path: '/create/athlete',
      component: CreateAthleteView
    },
    {
      path: '/create/club',
      component: CreateClubView
    },
    // {
    //   path: '/create/competition',
    //   component: CreateCompetitionView
    // },
    /*{
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    }*/
  ]
})

export default router
