export default class Competition {
    public starttime;
    public endtime
    constructor(
        public competitionname: string,
        starttime: Date,
        endtime: Date,
        public sportid: string | number,
        public competitionid: number
    ) {
        this.starttime = new Date(starttime);
        this.endtime = new Date(endtime);
    }
}