export default class Athlete {
    public birthday;

    constructor(
        public firstname: string,
        public lastname: string,
        birthday: Date,
        public athleteid: number
    ) {
        this.birthday = new Date(birthday)
    }
}