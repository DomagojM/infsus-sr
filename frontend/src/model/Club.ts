export default class Club {
    constructor(
        public clubname: string,
        public clubprefix: string,
        public sportid: string | number,
        public clubid: string
    ) {

    }
}