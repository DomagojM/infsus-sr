import Athlete from '@/model/Athlete';
import { defineStore } from 'pinia'

export const useAthleteListStore = defineStore({
    id: 'athleteList',
    state: () => ({
        active: true,
        rawItems: [] as Athlete[],
    }),
    getters: {
        status: (state) => state.active,
        items: (state) => state.rawItems.slice(),
    },
    actions: {
        async search(name: string) {
            let res = await fetch("/api/athletes?filter=" + name);
            if(res.status == 200)
                this.rawItems = await res.json();
            else
                this.rawItems = []
        },
  
        toggleActive() {
            this.active = !this.active
        },
    },
  })
