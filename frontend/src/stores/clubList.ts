import Club from '@/model/Club';
import { defineStore } from 'pinia'

export const useClubListStore = defineStore({
    id: 'clubList',
    state: () => ({
        active: false,
        rawItems: [] as Club[],
    }),
    getters: {
        status: (state) => state.active,
        items: (state) => state.rawItems.slice(),
    },
    actions: {
        async search(name: string) {
            let res = await fetch("/api/clubs?filter=" + name);
            if(res.status == 200)
                this.rawItems = await res.json();
            else
                this.rawItems = []
        },
  
        toggleActive() {
            this.active = !this.active
        },
    },
  })
