import Competition from '@/model/Competition';
import { defineStore } from 'pinia'

export const useCompetitionListStore = defineStore({
    id: 'competitionList',
    state: () => ({
        active: false,
        rawItems: [] as Competition[],
    }),
    getters: {
        status: (state) => state.active,
        items: (state) => state.rawItems.slice(),
    },
    actions: {
        async search(name: string) {
            let res = await fetch("/api/competitions?filter=" + name);
            if(res.status == 200)
                this.rawItems = await res.json();
            else
                this.rawItems = []
        },
  
        toggleActive() {
            this.active = !this.active
        },
    },
  })
